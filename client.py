#!/usr/bn/env python3
# Echo client program
import pickle
import threading
import socket
import time
import subprocess
import sys
from threading import Thread
import logging
import sys
import argparse

# Состояние 1 - ожидание задания
# Состояние 2 - в процессе работы
# Состояние 3 - процесс завершился


class Messages():
    Const_Waiting_time = 60
    Const_buf = 1024
    Const_Config_Working = bytes('2', 'ascii')
    Const_Config_Ended = bytes('3', 'ascii')
    server = 0

    def __init__(self, sock):
        self.sock = sock

    def send_str_data(self, string):
        self.sock.sendall(bytes(string, 'ascii'))

    def get_answer(self):
        self.sock.settimeout(self.Const_Waiting_time)
        Answer = str(self.sock.recv(self.Const_buf))
        #print(Answer)
        if Answer:
            return True
        return False

    def send_bytes(self, data):
        self.sock.sendall(data)


class CLIENT:
    sock = None
    Message = 0
    Limit = 0
    Const_Waiting_time = 60
    Const_buf = 1024
    Const_Config_Working = bytes('2', 'ascii') # Состояние 2 - в процессе работы
    Const_Config_Ended = bytes('3', 'ascii') # Состояние 3 - процесс завершился
    Cpu = 0
    Memory = 0

    def __init__(self, ID, HOST, PORT, Limit, Cpu, Memory):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((HOST, PORT))
        self.Limit = Limit
        self.Cpu = Cpu
        self.Memory = Memory
        self.Message = Messages(self.sock)
        self.start_work(ID, Cpu, Memory)

    def send_delete(self, Del):
        data = pickle.dumps(Del, pickle.HIGHEST_PROTOCOL)
        self.Message.send_str_data(str(len(data)))
        flag = self.Message.get_answer()
        if flag:
            self.Message.send_bytes(data)
            print("----------------------------------------")
            print("These tasks ended: ", Del)

    def working(self, tasks, Processes, Result, Count):
        Del = []
        print("----------------------------------------")
        print("Processes ", Processes)
        for string in Processes:
            thread_of_task = Processes[string]
            if thread_of_task.poll() == 0:
                Del.append(string)
            elif thread_of_task.poll() == 1:
                Count[string] += 1
                if Count[string] <= self.Limit:
                    Processes[string] = subprocess.Popen(tasks[string])
                else:
                    Result[string] = 1
                    Del.append(string)
        for string in Del:
            #print(string)
            Processes.pop(string)
            tasks.pop(string)
            Count.pop(string)
        self.send_delete(Del)
        return tasks, Processes, Result, Count

    def do_that_tasks(self, tasks, Processes, Result, Count):
        c = time.time()
        self.sock.sendall(self.Const_Config_Working)
        #print("Config 2")
        v = time.time()
        while v - c < 5:
            v = time.time()
        c = v
        tasks, Processes, Result, Count = self.working(
            tasks, Processes, Result, Count)
        #print("ended do_that_tasks")
        
        return tasks, Processes, Result, Count

    def add_that_tasks(self, tasks, Processes, Result, Count, string, task):
        Count[string] = 0
        Result[string] = None
        Processes[string] = subprocess.Popen(task.split())
        tasks[string] = task
        tasks, Processes, Result, Count = self.do_that_tasks(
            tasks, Processes, Result, Count)
        return tasks, Processes, Result, Count

    def read(self, num):
        v = self.sock.recv(self.Const_buf)
        num -= len(v)
        while num:
            z = self.sock.recv(self.Const_buf)
            v += z
            num -= len(z)
        return v

    def get_some_tasks(self, tasks, Processes, Result, Count):
        c = time.time()
        v = time.time()
        while v - c < 1:
            v = time.time()
        num = self.sock.recv(self.Const_buf).decode()
        if int(num) != 0: 
            self.sock.sendall(b'OK')
            v = self.read(int(num))
            print("----------------------------------------")
            print("Recieved data from server")
            data = pickle.loads(v)
            string = data[0]
            data = data[1]
            tasks, Processes, Result, Count = self.add_that_tasks(
                tasks, Processes, Result, Count, string, data)
        else:
            tasks, Processes, Result, Count = self.do_that_tasks(
                tasks, Processes, Result, Count)
                
        return tasks, Processes, Result, Count

    def start_work(self, ID, Cpu, Memory):
        array = [ID, Cpu, Memory]
        data = pickle.dumps(array, pickle.HIGHEST_PROTOCOL)
        N = bytes(str(len(data)), 'ascii')
        self.sock.sendall(N)
        self.sock.recv(self.Const_buf)
        self.sock.sendall(data)
        Processes = {}
        Result = {}
        Count = {}
        tasks = {}
        while True:
            tasks, Processes, Result, Count = self.get_some_tasks(
                tasks, Processes, Result, Count)


def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ID', default='1')
    parser.add_argument('--Host', default='127.0.0.1')
    parser.add_argument('--Port', type=int, default=5023)
    parser.add_argument('--Limit', type=int, default=5)
    parser.add_argument('--Cpu', type=int, default=0)
    parser.add_argument('--Memory', type=int, default=0)
    return parser


def start():
    parser = createParser()
    namespace = parser.parse_args()
    ID, HOST, PORT, Limit = namespace.ID, namespace.Host, namespace.Port, namespace.Limit
    # Cpu, Memory = namespace.Cpu, namespace.Memory
    print("----------------------------------------")
    print("Your ID ", ID)
    Client = CLIENT(ID, HOST, PORT, Limit, namespace.Cpu, namespace.Memory)


if __name__ == "__main__":
    start()
