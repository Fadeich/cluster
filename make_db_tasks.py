import sqlite3 as lite
print("input path")
path = input()
con = lite.connect(path + '/cluster/opt/Tasks.db')
with con:
    cur = con.cursor()
    cur.execute("DROP TABLE IF EXISTS Tasks")
    cur.execute("CREATE TABLE Tasks(Id, Num, Cpu, Memory, Task, Taken)")
    print("number of clients")
    N = int(input())
    for j in range(N):
        print(j, "Cpu, Memory, Task")
        line = input().split()
        i = 0
        while line:
            cur.execute("INSERT INTO Tasks VALUES( " + str(j + 1) + ", " + str(i+1) + ", " + str(line[0]) + ", " + str(line[1]) + ", " + str(line[2]) + ", 0)")
            print(str(j + 1) + ", " + str(i+1) + ", " + line[0] + ", " + line[1] + ", " + line[2])
            line = input().split()
            i += 1
    cur.execute('SELECT * FROM Tasks')
    line = cur.fetchall()
    for l in line:
        print(l)
    con.commit()
