#!/usr/bn/env python3
# Echo server program
import socket
import threading
import socketserver
from threading import Thread
import pickle
import sys
import argparse
import sqlite3 as lite
# Состояние 1 - ожидание задания
# Состояние 2 - в процессе работы
# Состояние 3 - процесс завершился


def put_tasks_done(Array_of_done, path):
    con = lite.connect(path + '/cluster/etc/cluster_map.db')
    with con:
        cur = con.cursor()
        for task in Array_of_done:
            # ord INT, Id, Num, Started, Done, Node
            task = task.split()
            print("----------------------------------------")
            print("set delete: ID, Num", task[0], task[1])
            cur.execute("UPDATE cluster_map SET Done=1 WHERE Id=" +
                        str(task[0]) + " and Num=" + str(task[1]))


class Messages():
    Const_Waiting_time = 60
    Const_buf = 1024
    Const_Config_Working = bytes('2', 'ascii') # Состояние 2 - в процессе работы
    Const_Config_Ended = bytes('3', 'ascii') # Состояние 3 - процесс завершился
    server = 0

    def __init__(self, server):
        self.server = server

    def put_Config(self, ID, Con, path):
        con = lite.connect(path + '/cluster/etc/Config.db')
        with con:
            cur = con.cursor()
            cur.execute("UPDATE Config SET Config=? WHERE Id=?", (Con, ID))
            con.commit()
            if not cur.rowcount:
                cur.execute(
                    "INSERT INTO Config VALUES(" + str(ID) + ", " +
                    str(Con) + ")")
                con.commit()

    def Node_fell(self, ID, path):
        con = lite.connect(path + '/cluster/etc/Config.db')
        with con:
            cur = con.cursor()
            print("----------------------------------------")
            print("fallen node: ", ID)
            cur.execute("UPDATE Config SET Config=-1 WHERE Id=" + str(ID))
            con.commit()

    def get_config(self, ID, path):
        self.server.request.settimeout(self.Const_Waiting_time)
        try:
            data = int(str(self.server.request.recv(self.Const_buf), 'ascii'))
            if not data:
                return None
            else:
                self.put_Config(ID, int(data), path)
                print("----------------------------------------")
                print("Got configuration from Node with ID", ID, int(data))
                return int(data)
        except:
            self.Node_fell(ID, path)
            return -1

    def send_str_data(self, string, ID, path):
        try:
            self.server.request.sendall(bytes(string, 'ascii'))
            return 1
        except:
            self.Node_fell(ID, path)
            return 0

    def get_answer(self, ID, path):
        self.server.request.settimeout(self.Const_Waiting_time)
        try:
            Answer = str(self.server.request.recv(self.Const_buf))
            if Answer:
                return True
            return False
        except:
            self.Node_fell(ID, path)
            return -1

    def send_bytes(self, data, ID, path):
        try:
            self.server.request.sendall(data)
            return 1
        except:
            self.Node_fell(ID, path)
            return 0

    def read(self, num, ID, path):
        try:
            v = self.server.request.recv(self.Const_buf)
            num -= len(v)
            while num:
                z = self.server.request.recv(self.Const_buf)
                v += z
                num -= len(z)
            return v
        except:
            self.Node_fell(ID, path)
            return 0

    def get_delete(self, ID, path):
        try:
            num = self.server.request.recv(self.Const_buf).decode()
            self.server.request.sendall(b'OK')
            v = self.read(int(num), ID, path)
            print("----------------------------------------")
            print("Got tasks that ended on Node with Id", ID)
            data = pickle.loads(v)
            print("numbers of tasks that ended", data)
            put_tasks_done(data, path)
            return 1
        except:
            self.Node_fell(ID, path)
            return 0


def find_ID_in_cluster_map(ID, path):
    con = lite.connect(path + '/cluster/etc/cluster_map.db')
    with con:
        cur = con.cursor()
        cur.execute(
            'SELECT * FROM cluster_map WHERE Started = 0 and Node = ' +
            str(ID) +
            ' ORDER BY ord')
        line = cur.fetchone()
        if not line or not len(line):
            return None
        # ord INT, Id, Num, Started, Done, Node
        cur.execute(
            "UPDATE cluster_map SET Started = 1 WHERE ord=?", (line[0], ))
        return int(line[2]), int(line[1])


def get_task(num_of_task, ID_of_client, path):
    File = open(path + "/cluster/opt/" + str(ID_of_client) + 'ID_tasks.txt')
    lines = [line for line in File]
    line = lines[num_of_task - 1]
    return line


def add_nod_to_Resource(ID, Cpu, Memory, path):
    # Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks
    con = lite.connect(path + '/cluster/opt/Resource.db')
    with con:
        cur = con.cursor()
        cur.execute(
            "INSERT INTO Resource VALUES(" +
            str(ID) +
            ", " +
            str(Cpu) +
            ", " +
            str(Memory) +
            ", 0, 0, 0)")
        cur.execute('SELECT * FROM Resource WHERE Id=0')
        line = cur.fetchone()
        cur.execute("UPDATE Resource SET Cpu=" +
                    str(int(line[1]) + Cpu) + " WHERE Id=0")
        cur.execute("UPDATE Resource SET Memory=" +
                    str(int(line[2]) + Memory) + " WHERE Id=0")
        print("----------------------------------------")
        print("added nod: ID, CPU, Mem", ID, Cpu, Memory)
        con.commit()


class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    Const_Waiting_time = 60
    Const_buf = 1024

    def method(self, ID, path):
        Message = Messages(self)
        while True:
            Arr = find_ID_in_cluster_map(ID, path)
            print("----------------------------------------")
            print("Num and ID of the task, that should be done", Arr)
            if Arr:
                num_of_task = Arr[0]
                ID_of_client = Arr[1]
                line = get_task(num_of_task, ID_of_client, path)
                string = str(ID_of_client) + " " + str(num_of_task)
                line = [string, line]
                data = pickle.dumps(line, pickle.HIGHEST_PROTOCOL)
                if not Message.send_str_data(str(len(data)), ID, path):
                    return
                flag = Message.get_answer(ID, path)
                if flag == -1:
                    return
                if flag:
                    if not Message.send_bytes(data, ID, path):
                        return
                else:
                    return
            else:
                self.request.sendall(b'0')
            if Message.get_config(ID, path) == -1:)
                return
            if not Message.get_delete(ID, path):
                return

    def read(self, num):
        v = self.request.recv(self.Const_buf)
        num -= len(v)
        while num:
            z = self.request.recv(self.Const_buf)
            v += z
            num -= len(z)
        return v

    def handle(self):
        path_in_file = open("path.txt")
        path = path_in_file.readline()
        N = int(str(self.request.recv(self.Const_buf), 'ascii'))
        self.request.sendall(b'OK')
        array = pickle.loads(self.read(N))
        print("----------------------------------------")
        print("Got from Node ID, Cpu, Memory:", array)
        add_nod_to_Resource(array[0], array[1], array[2], path)
        t1 = Thread(target=self.method(array[0], path))
        t1.start()
        t1.join()
        print("----------------------------------------")
        print("end of this thread")
        print("----------------------------------------")


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--Port', type=int, default=5023)
    parser.add_argument('--Path', default="/home/anastasia")
    return parser


def make_a_bd(path):
    print("Making the data base with Configurations")
    con = lite.connect(path + '/cluster/etc/Config.db')
    with con:
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS Config")
        cur.execute("CREATE TABLE Config(Id INT, Config)")


def start():
    parser = createParser()
    namespace = parser.parse_args()

    server = ThreadedTCPServer(('', namespace.Port), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    make_a_bd(namespace.Path)
    server_thread = threading.Thread(
        target=server.serve_forever(), daemon=True)


if __name__ == "__main__":
    start()
