#!/usr/bn/env python3
import sqlite3 as lite
import sys
import argparse


class Resource_DB:
    con = 0
    cur = 0

    def __init__(self, path):
        self.con = lite.connect(path + '/cluster/opt/Resource.db')

    def make_a_bd_Resource(self, path):
        with self.con:
            cur = self.con.cursor()
            cur.execute("DROP TABLE IF EXISTS Resource")
            cur.execute(
                "CREATE TABLE Resource(Id, Cpu, Memory, Cpu_in_work, " +
                "Memory_in_work, tasks)")
            cur.execute("INSERT INTO Resource VALUES(0, 0, 0, 0, 0, 0)")
            self.con.commit()

    def make_a_bd_resource(self, path):
        with self.con:
            cur = self.con.cursor()
            cur.execute("DROP TABLE IF EXISTS Resource")
            cur.execute(
                "CREATE TABLE Resource(Id, Cpu, Memory, Cpu_in_work, " +
                "Memory_in_work, tasks)")
            cur.execute("INSERT INTO Resource VALUES(0, 0, 0, 0, 0, 0)")
            self.con.commit()

    def Update_db_resource(self, ID, line, path):
        # Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks
        with self.con:
            cur = self.con.cursor()
            cur.execute('SELECT * FROM Resource WHERE ID=' + str(ID))
            old_line = cur.fetchone()
            cur.execute(
                "UPDATE Resource SET Cpu_in_work=?, Memory_in_work=?, " +
                "tasks=? WHERE Id=?",
                (old_line[3] - line[0],
                 old_line[4] - line[1],
                    old_line[5] - 1,
                    ID))
            cur.execute('SELECT * FROM Resource WHERE ID=0')
            old_line = cur.fetchone()
            cur.execute(
                "UPDATE Resource SET Cpu_in_work=?, Memory_in_work=?, " +
                "tasks=? WHERE Id=0",
                (old_line[3] - line[0],
                 old_line[4] - line[1],
                    old_line[5] - 1))
            self.con.commit()

    def find_Resource(self, path):
        with self.con:
            while True:
                cur = self.con.cursor()
                # Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks
                cur.execute('SELECT * FROM Resource WHERE Id=0')
                #print("no resourse")
                line = cur.fetchone()
                if line:
                    if int(line[1]) and int(line[2]):
                        break
        return [int(line[1]), int(line[2])]

    def find_Consumed_resources(self, path):
        with self.con:
            cur = self.con.cursor()
            # Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks
            cur.execute('SELECT * FROM Resource WHERE Id = 0')
            line = cur.fetchone()
        return [int(line[3]), int(line[4])]

    def find_nod(self, Demand, path):
        # Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks
        with self.con:
            cur = self.con.cursor()
            cur.execute('SELECT * FROM Resource ORDER BY tasks')
            line = cur.fetchone()
            print("Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks", line)
            while line:
                if int(line[1]) - int(line[3]) >= int(Demand[0]) and int(line[2]) - int(line[4]) >= int(Demand[1]) and int(line[0]):
                    return int(line[0])
                line = cur.fetchone()

    def change_resource(self, Nod, Demand, path):
        # Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks
        with self.con:
            cur = self.con.cursor()
            cur.execute('SELECT * FROM Resource WHERE ID=' + str(Nod))
            old_line = cur.fetchone()
            cur.execute("UPDATE Resource SET Cpu_in_work=?, " +
                        "Memory_in_work=?, tasks=? WHERE Id=?",
                        (int(old_line[3]) + int(Demand[0]),
                         int(old_line[4]) + int(Demand[1]),
                         int(old_line[5]) + 1, Nod))
            cur.execute('SELECT * FROM Resource WHERE ID=0')
            old_line = cur.fetchone()
            cur.execute("UPDATE Resource SET Cpu_in_work=?, " +
                        "Memory_in_work=?, tasks=? WHERE Id=0",
                        (int(old_line[3]) + int(Demand[0]),
                         int(old_line[4]) + int(Demand[1]),
                            int(old_line[5]) + 1))
            self.con.commit()

    def change_Resource(self, Consumed_resources, ID, path):
        # Id, Cpu, Memory, Cpu_in_work, Memory_in_work, tasks
        with self.con:
            cur = self.con.cursor()
            cur.execute('SELECT * FROM Resource WHERE ID=' + str(ID))
            old_line = cur.fetchone()
            Consumed_resources = [Consumed_resources[0] - int(old_line[3]),
                                  Consumed_resources[0] - int(old_line[4])]
            cur.execute('SELECT * FROM Resource WHERE ID=0')
            all_res = cur.fetchone()
            cur.execute("UPDATE Resource SET Cpu=?, Memory=?, " +
                        "Cpu_in_work=?, " +
                        "Memory_in_work=?, tasks=? WHERE Id=0",
                        (int(all_res[1]) - int(old_line[1]),
                         int(all_res[2]) - int(old_line[2]),
                            int(all_res[3]) - int(old_line[3]),
                            int(all_res[4]) - int(old_line[4]),
                            int(all_res[5]) - int(old_line[5])))
            cur.execute('DELETE FROM Resource WHERE ID=' + str(ID))
            self.con.commit()
            return Consumed_resources


class Tasks_DB:
    con = 0
    cur = 0

    def __init__(self, path):
        self.con = lite.connect(path + '/cluster/opt/Tasks.db')
        
    def find_demand_for_done(self, ID, Num, path):
        with self.con:
            # Id, Num, Cpu, Memory, Task, Taken)
            cur = self.con.cursor()
            cur.execute('SELECT * FROM Tasks WHERE Id=? AND Num=?', (ID, Num))
            line = cur.fetchone()
            if not line or not len(line):
                print("no ID, Num:", ID, Num)
                return None
            return [int(line[2]), int(line[3])]

    def find_demand(self, Share_dominant, path):
        array = []
        for i in range(len(Share_dominant)):
            if Share_dominant[i] == min(Share_dominant):
                array.append(i)
        with self.con:
            # Id, Num, Cpu, Memory, Task, Taken)
            cur = self.con.cursor()
            for ID in array:
                cur.execute('SELECT * FROM Tasks WHERE Taken = 0 and ID = ' +
                            str(ID + 1) + ' ORDER BY Num')
                line = cur.fetchone()
                if line:
                    return [int(line[2]), int(line[3])], ID, int(line[1])
            return None, None, None

    def set_taken(self, ID, path):
        with self.con:
            # Id, Num, Cpu, Memory, Task, Taken)
            cur = self.con.cursor()
            cur.execute('SELECT * FROM Tasks WHERE Taken = 0 and ID = ' +
                        str(ID + 1) + ' ORDER BY Num')
            line = cur.fetchone()
            cur.execute(
                "UPDATE Tasks SET Taken = 1 WHERE ID=? and Num=?",
                (line[0],
                line[1],
                ))
            self.con.commit()

    def change_Tasks(self, Id, Num, path):
        with self.con:
            # Id, Num, Cpu, Memory, Task, Taken)
            cur = self.con.cursor()
            cur.execute(
                "UPDATE Tasks SET Taken = 0 WHERE ID=? and Num=?", (Id, Num, ))
            print("----------------------------------------")
            print("Not taken yet, because the node fell: ID, Num", Id, Num)
            self.con.commit()

    def find_num_of_task(self, path, ind):
        with con:
            # Id, Num, Cpu, Memory, Task, Taken)
            cur = self.con.cursor()
            cur.execute(
                "SELECT Num FROM Tasks WHERE ID=? and Taken = 0 " +
                "ORDER BY Num", (ind + 1, ))
            self.con.commit()
            num = cur.fetchone()
            print("----------------------------------------")
            print("Num of next task is ", num)
            return int(num[0])


class Cluster_map_DB:
    con = 0
    cur = 0

    def __init__(self, path):
        self.con = lite.connect(path + '/cluster/etc/cluster_map.db')

    def make_a_bd(self, path):
        with self.con:
            cur = self.con.cursor()
            cur.execute("DROP TABLE IF EXISTS cluster_map")
            cur.execute(
                "CREATE TABLE cluster_map(ord INT, Id, Num, Started, Done, Node)")

    def put_task(self, order, ID, num, Nod, path):
        cur = self.con.cursor()
        with self.con:
            # ord, Id, Num, Started, Done, Node
            cur.execute(
                "INSERT INTO cluster_map VALUES(" +
                str(order) +
                ", " +
                str(ID) +
                ", " +
                str(num) +
                ", 0, 0, " +
                str(Nod) +
                ")")
            print("----------------------------------------")
            print("Added to cluster_map: order, ID, num, Nod", order, ID, num, Nod)
            self.con.commit()

    def Update_cluster_map(self, path):
        cur = self.con.cursor()
        with self.con:
            cur.execute("UPDATE cluster_map SET Done=-1 WHERE Done=1")
            #print("updated cluster_map with done")
            self.con.commit()
    
    def Chec_cluster_map(self, path):
        cur = self.con.cursor()
        with self.con:
            cur.execute('SELECT * FROM cluster_map WHERE Done=1')
            z = cur.fetchall()
            if z:
                print("put 3 for ord, Id, Num, Started, Done, Node", z)
                return True


    def Update_resource_info(
        self,
        DB_Tasks,
        DB_Res,
        Users_resources,
        Share_dominant,
        Consumed_resources,
        Resource,
        path):
        cur = self.con.cursor()
        with self.con:
            # ord, Id, Num, Started, Done, Node
            cur.execute('SELECT * FROM cluster_map WHERE Done=1')
            Done = cur.fetchall()
            for done in Done:
                print("----------------------------------------")
                print("ord INT, Id, Num, Started, Done, Node")
                print("Ended task: ", done)
                cur.execute("UPDATE cluster_map SET Done=-1 WHERE Id="+str(done[1])+" and Num="+str(done[2]))
                ID = int(done[1])
                Num = int(done[2])
                Node = int(done[5])
                line = DB_Tasks.find_demand_for_done(
                    ID, Num, path)
                Users_resources[ID - 1] = sub(Users_resources[ID - 1], line)
                Consumed_resources = sub(Consumed_resources, line)
                Share_dominant[
                    ID -
                    1] = find_max(
                    Users_resources[
                        ID -
                        1],
                    Resource)
                DB_Res.Update_db_resource(Node, line, path)
            return Users_resources, Share_dominant, Consumed_resources

    def change_Cluster_map(self, ID, path):
        # ord, Id, Num, Started, Done, Node
        cur = self.con.cursor()
        with self.con:
            cur.execute(
                'SELECT * FROM cluster_map WHERE Node=' +
                str(ID) +
                ' and Done=0')
            array = cur.fetchall()
            cur.execute(
                'DELETE FROM cluster_map WHERE Node=' +
                str(ID) +
                ' and Done=0')
            return array


def find_fallen_nodes(path):
    con = lite.connect(path + '/cluster/etc/Config.db')
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM Config WHERE Config=-1")
        array = cur.fetchall()
        #cur.execute("DELETE FROM Config WHERE Config=-1")
        con.commit()
        return array

def del_fallen_nodes(path):
    con = lite.connect(path + '/cluster/etc/Config.db')
    with con:
        cur = con.cursor()
        cur.execute("DELETE FROM Config WHERE Config=-1")
        con.commit()



def Check_access_to_nodes(DB_Tasks, Cluster_map, DB_Res, Consumed_resources, path):
    num_of_not_done_tasks = 0
    for node in find_fallen_nodes(path):
        print("----------------------------------------")
        print("fallen node: ", node[0])
        del_fallen_nodes(path)
        Consumed_resources = DB_Res.change_Resource(
            Consumed_resources, node[0], path)
        for line in Cluster_map.change_Cluster_map(node[0], path):
            print("Num ", line[0])
            # change_Tasks(node[0], line[0], path) ###id of client, not of node
            DB_Tasks.change_Tasks(line[1], line[2], path)
            num_of_not_done_tasks += 1
    return Consumed_resources, num_of_not_done_tasks


# Algorithm DRF

def check(Consumed_resources, Demand_ind, Resource):
    for i in range(len(Consumed_resources)):
        if Consumed_resources[i] + int(Demand_ind[i]) > Resource[i]:
            return False
    return True


def Sum_of_Lists(Consumed_resources, Demand_ind):
    Sum = [0 for k in range(len(Consumed_resources))]
    for i in range(len(Consumed_resources)):
        Sum[i] = Consumed_resources[i] + int(Demand_ind[i])
    return Sum


def find_max(Users_resources, Resource):
    i = 0
    for j in range(1, len(Users_resources)):
        if Users_resources[i] * Resource[j] < Users_resources[j] * Resource[i]:
            i = j
    return Users_resources[i] / Resource[i]


def sub(Users_resources, line):
    S = [0 for k in range(len(line))]
    for i in range(len(line)):
        S[i] = Users_resources[i] - int(line[i])
    return S


def DRF(N, path):
    DB_Res = Resource_DB(path)
    Cluster_map = Cluster_map_DB(path)
    DB_Tasks = Tasks_DB(path)
    DB_Res.make_a_bd_resource(path)
    # [CPU, Memory] total resource capasities
    Resource = DB_Res.find_Resource(path)
    Consumed_resources = DB_Res.find_Consumed_resources(
        path)  # consumed resources
    Share_dominant = [0 for k in range(N)]  # dominant shares
    Users_resources = [[0, 0] for k in range(N)]  # users resoures
    Num_of_task = [0 for p in range(N)]
    Cluster_map.make_a_bd(path)
    counter_of_order = 1
    flag = True
    while True:
        Demand_ind, ind, Num_of_task = DB_Tasks.find_demand(
            Share_dominant, path)  # File_of_resource[ind].readline().split()
        if Demand_ind:
            # print(Consumed_resources, Demand_ind, Resource)
            if check(Consumed_resources, Demand_ind, Resource):
                DB_Tasks.set_taken(ind, path)
                print("----------------------------------------")
                print("Num of Task to be done next", Num_of_task)
                Nod = DB_Res.find_nod(Demand_ind, path)
                if Nod:
                    Cluster_map.put_task(counter_of_order, ind + 1, Num_of_task, Nod, path)
                    DB_Res.change_resource(Nod, Demand_ind, path)
                    counter_of_order += 1
                    Consumed_resources = Sum_of_Lists(
                        Consumed_resources, Demand_ind)
                    Users_resources[ind] = Sum_of_Lists(
                        Users_resources[ind], Demand_ind)
                    Share_dominant[ind] = find_max(
                        Users_resources[ind], Resource)
        else:
            if flag:
                print("----------------------------------------")
                print("no new tasks")
                flag = False
            Consumed_resources = DB_Res.find_Consumed_resources(path)
        Consumed_resources, num_of_not_done_tasks = Check_access_to_nodes(
            DB_Tasks, Cluster_map, DB_Res, Consumed_resources, path)
        counter_of_order -= num_of_not_done_tasks
        Resource = DB_Res.find_Resource(path)
        Users_resources, Share_dominant, Consumed_resources = Cluster_map.Update_resource_info(
                             DB_Tasks, DB_Res, Users_resources,
                             Share_dominant, Consumed_resources,
                             Resource, path)


def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--Path', default="/home/anastasia")
    parser.add_argument('--Number_of_Clients', type=int, default=1)
    return parser


def start():
    parser = createParser()
    namespace = parser.parse_args()
    path_in_file = open(namespace.Path + "/cluster/bin/path.txt", 'w+')
    path_in_file.write(namespace.Path)
    path_in_file.close()
    DRF(namespace.Number_of_Clients, namespace.Path)


if __name__ == "__main__":
    start()
